﻿using Microsoft.Build.Framework;
using Newtonsoft.Json;

namespace QuickLauncher
{
	public class Application
	{
		public string Name;
		public string Path;
		public string Group;
		public bool IsAdminMode;
	}
}
