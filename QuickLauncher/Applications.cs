﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace QuickLauncher
{
	public class Applications
	{
		private const string ApplicationsFileName = "Applications.json";

		private List<Application> _applications;

		public Applications()
		{
			_applications = new List<Application>();
			_applications = Load();
		}

		public void Add(Application application)
		{
			_applications.Add(application);
			Save();
		}

		public void Remove(int index)
		{
			_applications.RemoveAt(index);
			Save();
		}

		public void Edit(Application newApplciationData, int applicationToChangeIndex)
		{
			_applications[applicationToChangeIndex].Name = newApplciationData.Name;
			_applications[applicationToChangeIndex].Path = newApplciationData.Path;
			_applications[applicationToChangeIndex].Group = newApplciationData.Group;
			_applications[applicationToChangeIndex].IsAdminMode = newApplciationData.IsAdminMode;

			Save();
		}

		public int FindIndexOf(Application lookingApplciation)
		{
			for (var index = 0; index < _applications.Count; index++)
			{
				if (_applications[index].Name.Equals(lookingApplciation.Name))
				{
					return index;
				}
			}

			return -1;
		}

		public List<Application> Load()
		{
			var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + ApplicationsFileName);
			if (File.Exists(path))
			{
				using (StreamReader reader = new StreamReader(path))
				{
					string jsonFromFile = reader.ReadToEnd();
					_applications = JsonConvert.DeserializeObject<List<Application>>(jsonFromFile);
				}
			}

			return _applications;
		}

		public void Save()
		{
			var json = JsonConvert.SerializeObject(_applications, Formatting.Indented);
			var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + ApplicationsFileName);

			using (StreamWriter writer = new StreamWriter(path))
			{
				writer.Write(json);
			}
		}

		public List<Application> GetApplications()
		{
			return _applications;
		}

		public string SetName(string name, string path)
		{

			return string.IsNullOrWhiteSpace(name) ? Path.GetFileNameWithoutExtension(path) : name;
		}

		public bool IsUsedName(string name)
		{
			foreach (var application in _applications)
			{
				if (application.Name.Equals(name))
				{
					return true;
				}
			}
			return false;
		}
	}
}