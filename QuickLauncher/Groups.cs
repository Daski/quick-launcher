﻿using System;
using System.Collections.Generic;
using System.IO;

namespace QuickLauncher
{
	public class Groups
	{
		private const string GroupsFileName = "Groups.txt";

		private readonly List<string> _groups;

		public Groups()
		{
			_groups = new List<string>();
			_groups = Load();
		}

		public void Add(string group)
		{
			if (!_groups.Contains(group))
			{
				_groups.Add(group);
			}

			Save();
		}

		public void Save()
		{
			var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + GroupsFileName);

			using (StreamWriter writer = new StreamWriter(path, false))
			{
				foreach (var group in _groups)
				{
					writer.WriteLine(group);
				}
			}
		}

		public List<string> Load()
		{
			var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + GroupsFileName);
			var groups = new List<string>();

			if (File.Exists(path))
			{
				using (StreamReader reader = new StreamReader(path))
				{
					string line;
					while ((line = reader.ReadLine()) != null)
					{
						groups.Add(line);
					}
				}
			}

			return groups;
		}

		public List<string> GetGroups()
		{
			return _groups;
		}
	}
}