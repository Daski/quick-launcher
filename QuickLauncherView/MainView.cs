﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using QuickLauncher;
using Application = QuickLauncher.Application;

namespace QuickLauncherView
{
	public partial class MainView : Form
	{
		private readonly Applications _applicationsController;

		public MainView()
		{
			_applicationsController = Controller.GetApplicationsReference();

			InitializeComponent();
			InitilizeApplicationsView();

			FillApplciationView();
		}

		private void InitilizeApplicationsView()
		{
			applicationsView.View = View.Details;
			applicationsView.FullRowSelect = true;
			applicationsView.MultiSelect = false;
			SetEventsForApplicationView();
			PrepareColumns();
		}

		#region Initilize Application View
		private void PrepareColumns()
		{
			applicationsView.Columns.Add("NameColumn", "Name");
			applicationsView.Columns.Add("PathColumn", "Path");
			applicationsView.Columns.Add("GroupColumn", "Group");

			applicationsView.Columns[0].Width = 200;
			applicationsView.Columns[1].Width = 400;
			applicationsView.Columns[2].Width = 200;
		}

		private void SetEventsForApplicationView()
		{
			applicationsView.MouseDoubleClick += applicationsViewRow_MouseDouble;
		}
		#endregion

		#region Buttons and usages
		private void NewApplication_Click(object sender, System.EventArgs e)
		{
			EditView editView = new EditView();
			editView.ShowDialog();

			RefreshApplicationViewData();
		}

		private void Remove_Click(object sender, System.EventArgs e)
		{
			var index = _applicationsController.FindIndexOf(SelectedApplciation());

			if (index != -1)
			{
				_applicationsController.Remove(index);
			}

			RefreshApplicationViewData();
		}

		private void Close_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private Application SelectedApplciation()
		{
			ListViewItem item = applicationsView.SelectedItems[0];

			return new Application()
			{
				Name = item.SubItems[0].Text,
				Path = item.SubItems[1].Text,
				Group = item.SubItems[2].Text
			};
		}

		private void applicationsViewRow_MouseDouble(object sender, MouseEventArgs e)
		{
			var selectedApplciation = SelectedApplciation();

			EditView editView = new EditView();
			editView.IsEditMode = true;
			editView.FindIndexOfApplication(selectedApplciation);
			editView.Fill(selectedApplciation);
			editView.ShowDialog();

			RefreshApplicationViewData();
		}
		#endregion

		private void FillApplciationView()
		{
			List<Application> applications = _applicationsController.GetApplications();

			foreach (var application in applications)
			{
				applicationsView.Items.Add(new ListViewItem(new string[] { application.Name, application.Path, application.Group }));
			}
		}

		private void RefreshApplicationViewData()
		{
			applicationsView.Items.Clear();
			FillApplciationView();
			applicationsView.Refresh();
		}
	}
}
