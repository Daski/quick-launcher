﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using QuickLauncherView.Properties;

namespace QuickLauncherView
{
	internal class CustomApplicationContext : ApplicationContext
	{
		private const int SysErrorCancelled = 1223;

		private NotifyIcon _notifyIcon;

		public CustomApplicationContext()
		{
			InitializeNotifyIcon();
		}

		private void InitializeNotifyIcon()
		{
			_notifyIcon = new NotifyIcon()
			{
				Icon = Resources.AppIcon,
				ContextMenuStrip = PrepareMenu(),
				Visible = true
			};
		}

		private ContextMenuStrip PrepareMenu()
		{
			var contextMenu = new ContextMenuStrip();
			var applications = Controller.GetApplicationsReference().GetApplications();

			contextMenu.Items.Add(AddConfigOptionToMenu());

			foreach (var application in applications)
			{
				if (!string.IsNullOrWhiteSpace(application.Group))
				{
					ToolStripMenuItem group;

					if (!contextMenu.Items.ContainsKey(application.Group))
					{
						group = new ToolStripMenuItem(application.Group);
						group.Name = application.Group;
						group.DropDownItems.Add(AddNewItemToGroup(application));

						contextMenu.Items.Add(group);
					}
					else
					{
						var index = contextMenu.Items.IndexOfKey(application.Group);
						group = (ToolStripMenuItem)contextMenu.Items[index];
						group.DropDownItems.Add(AddNewItemToGroup(application));

						contextMenu.Items.Insert(index, group);
					}
				}
				else
				{
					contextMenu.Items.Add(AddNewItemToGroup(application));
				}
			}
			contextMenu.Items.Add(AddExitOptionToMenu());

			return contextMenu;
		}

		private ToolStripMenuItem AddNewItemToGroup(QuickLauncher.Application application)
		{
			ToolStripMenuItem item = new ToolStripMenuItem(application.Name, null, SelectedItem_Click);
			item.Name = application.Name;

			return item;
		}

		private ToolStripMenuItem AddConfigOptionToMenu()
		{
			var name = "Configuration";
			var item = new ToolStripMenuItem(name, Resources.configuration, Configuration);
			item.Name = name;
			item.Font = new Font(item.Font, FontStyle.Bold);

			return item;
		}

		private ToolStripMenuItem AddExitOptionToMenu()
		{
			var name = "Exit";
			var item = new ToolStripMenuItem(name, Resources.exit, Exit);
			item.Name = name;
			item.Font = new Font(item.Font, FontStyle.Bold);

			return item;
		}

		private void Update()
		{
			_notifyIcon.ContextMenuStrip = PrepareMenu();
		}

		#region Buttons and Events
		private void Exit(object sender, EventArgs e)
		{
			_notifyIcon.Visible = false;
			Application.Exit();
		}

		private void Configuration(object sender, EventArgs e)
		{
			using (var mainForm = new MainView())
			{
				mainForm.ShowDialog();
				Update();
			}
		}

		private void SelectedItem_Click(object sender, EventArgs e)
		{
			ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

			foreach (var application in Controller.GetApplicationsReference().GetApplications())
			{
				if (application.Name.Equals(menuItem.Text))
				{
					using (var process = new Process())
					{
						process.StartInfo.FileName = application.Path;

						if (application.IsAdminMode)
						{
							process.StartInfo.Verb = "runas";
						}

						StartProcess(process);
					}
				}
			}
		}

		private void StartProcess(Process process)
		{
			try
			{
				process.Start();
			}
			catch (Win32Exception ex)
			{
				if (ex.NativeErrorCode == SysErrorCancelled)
				{
					MessageBox.Show(Resources.SysError_OperationCancelledByUser, Resources.QuickLauncher_Name);
				}
				else
				{
					throw;
				}
			}
		}
		#endregion
	}
}