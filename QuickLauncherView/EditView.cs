﻿using System;
using System.IO;
using System.Windows.Forms;
using QuickLauncher;
using QuickLauncherView.Properties;
using Application = QuickLauncher.Application;

namespace QuickLauncherView
{
	public partial class EditView : System.Windows.Forms.Form
	{
		public bool IsEditMode;
		public bool IsAdminMode;

		#region Private Fields
		private readonly Applications _applications;
		private readonly Groups _groups;
		private int index;
		private Application _currentApplication;
		#endregion

		public EditView()
		{
			IsEditMode = false;
			_applications = Controller.GetApplicationsReference();
			_groups = Controller.GetGroupsReference();

			InitializeComponent();
			PrepareGroupsCombobox();
		}

		public void Fill(Application application)
		{
			_currentApplication = application;
			AppName.Text = application.Name;
			AppPath.Text = application.Path;
			AppGroup.Text = application.Group;
			isAdminMode.Checked = application.IsAdminMode;
		}

		public void FindIndexOfApplication(Application application)
		{
			index = _applications.FindIndexOf(application);
		}

		#region Buttons and Events
		private void Save_Click(object sender, EventArgs e)
		{
			var name = AppName.Text;
			var path = AppPath.Text;
			var group = AppGroup.Text;
			var adminMode = isAdminMode.Checked;

			if (CheckPath(path))
			{
				name = SetName(name, path);

				if (IsEditMode)
				{
					if (CheckNameAfterEdit(name))
					{
						_applications.Edit(new Application()
						{
							Name = name,
							Path = path,
							Group = group,
							IsAdminMode = adminMode
						}, index);

						_groups.Save();

						this.Close();
					}
					else
					{
						Info.Text = string.Format(Resources.Error_NameIsUsed, name);
					}
				}
				else
				{
					if (!_applications.IsUsedName(AppName.Text))
					{
						_applications.Add(new Application()
						{
							Name = name,
							Path = path,
							Group = group,
							IsAdminMode = adminMode
						});

						_groups.Add(group);

						this.Close();
					}
					else
					{
						Info.Text = string.Format(Resources.Error_NameIsUsed, name);
						AppName.Text = name;
					}
				}
			}
			else
			{
				Info.Text = Info.Text = Resources.Error_PathIsRequired;
			}
		}

		private void ButtonCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void ButtonBrowse_Click(object sender, EventArgs e)
		{
			using (OpenFileDialog openFileDialog = new OpenFileDialog())
			{
				openFileDialog.DefaultExt = ".exe";
				openFileDialog.Filter = "(.exe)|*.exe";
				openFileDialog.Multiselect = false;

				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					AppPath.Text = openFileDialog.FileName;
				}
			}
		}
		#endregion

		private string SetName(string name, string path)
		{
			return string.IsNullOrWhiteSpace(name) ? Path.GetFileNameWithoutExtension(path) : name;
		}

		private bool CheckNameAfterEdit(string newName)
		{
			if (_currentApplication.Name.Equals(newName))
			{
				return true;
			}
			return false;
		}

		private bool CheckPath(string path)
		{
			if (!string.IsNullOrWhiteSpace(path))
			{
				if (File.Exists(path))
				{
					return true;
				}
			}
			return false;
		}

		private void PrepareGroupsCombobox()
		{
			foreach (var group in _groups.GetGroups())
			{
				AppGroup.Items.Add(group);
			}
		}
	}
}
