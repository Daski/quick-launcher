﻿namespace QuickLauncherView
{
	partial class EditView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.LabelName = new System.Windows.Forms.Label();
			this.LabelPath = new System.Windows.Forms.Label();
			this.LabelGroup = new System.Windows.Forms.Label();
			this.AppName = new System.Windows.Forms.TextBox();
			this.AppPath = new System.Windows.Forms.TextBox();
			this.ButtonBrowse = new System.Windows.Forms.Button();
			this.Save = new System.Windows.Forms.Button();
			this.ButtonCancel = new System.Windows.Forms.Button();
			this.Info = new System.Windows.Forms.Label();
			this.AppGroup = new System.Windows.Forms.ComboBox();
			this.isAdminMode = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// LabelName
			// 
			this.LabelName.AutoSize = true;
			this.LabelName.Location = new System.Drawing.Point(13, 15);
			this.LabelName.Name = "LabelName";
			this.LabelName.Size = new System.Drawing.Size(38, 13);
			this.LabelName.TabIndex = 0;
			this.LabelName.Text = "Name:";
			// 
			// LabelPath
			// 
			this.LabelPath.AutoSize = true;
			this.LabelPath.Location = new System.Drawing.Point(13, 40);
			this.LabelPath.Name = "LabelPath";
			this.LabelPath.Size = new System.Drawing.Size(35, 13);
			this.LabelPath.TabIndex = 1;
			this.LabelPath.Text = "Path: ";
			// 
			// LabelGroup
			// 
			this.LabelGroup.AutoSize = true;
			this.LabelGroup.Location = new System.Drawing.Point(13, 66);
			this.LabelGroup.Name = "LabelGroup";
			this.LabelGroup.Size = new System.Drawing.Size(39, 13);
			this.LabelGroup.TabIndex = 2;
			this.LabelGroup.Text = "Group:";
			// 
			// AppName
			// 
			this.AppName.Location = new System.Drawing.Point(57, 12);
			this.AppName.Name = "AppName";
			this.AppName.Size = new System.Drawing.Size(282, 20);
			this.AppName.TabIndex = 3;
			// 
			// AppPath
			// 
			this.AppPath.Location = new System.Drawing.Point(58, 37);
			this.AppPath.Name = "AppPath";
			this.AppPath.Size = new System.Drawing.Size(281, 20);
			this.AppPath.TabIndex = 4;
			// 
			// ButtonBrowse
			// 
			this.ButtonBrowse.Location = new System.Drawing.Point(342, 37);
			this.ButtonBrowse.Name = "ButtonBrowse";
			this.ButtonBrowse.Size = new System.Drawing.Size(61, 21);
			this.ButtonBrowse.TabIndex = 6;
			this.ButtonBrowse.Text = "Browse";
			this.ButtonBrowse.UseVisualStyleBackColor = true;
			this.ButtonBrowse.Click += new System.EventHandler(this.ButtonBrowse_Click);
			// 
			// Save
			// 
			this.Save.Location = new System.Drawing.Point(57, 91);
			this.Save.Name = "Save";
			this.Save.Size = new System.Drawing.Size(75, 23);
			this.Save.TabIndex = 7;
			this.Save.Text = "Save";
			this.Save.UseVisualStyleBackColor = true;
			this.Save.Click += new System.EventHandler(this.Save_Click);
			// 
			// ButtonCancel
			// 
			this.ButtonCancel.Location = new System.Drawing.Point(139, 91);
			this.ButtonCancel.Name = "ButtonCancel";
			this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
			this.ButtonCancel.TabIndex = 8;
			this.ButtonCancel.Text = "Cancel";
			this.ButtonCancel.UseVisualStyleBackColor = true;
			this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
			// 
			// Info
			// 
			this.Info.AutoSize = true;
			this.Info.Location = new System.Drawing.Point(220, 96);
			this.Info.Name = "Info";
			this.Info.Size = new System.Drawing.Size(0, 13);
			this.Info.TabIndex = 9;
			// 
			// AppGroup
			// 
			this.AppGroup.FormattingEnabled = true;
			this.AppGroup.Location = new System.Drawing.Point(58, 64);
			this.AppGroup.Name = "AppGroup";
			this.AppGroup.Size = new System.Drawing.Size(281, 21);
			this.AppGroup.TabIndex = 10;
			// 
			// isAdminMode
			// 
			this.isAdminMode.AutoSize = true;
			this.isAdminMode.Location = new System.Drawing.Point(273, 97);
			this.isAdminMode.Name = "isAdminMode";
			this.isAdminMode.Size = new System.Drawing.Size(122, 17);
			this.isAdminMode.TabIndex = 11;
			this.isAdminMode.Text = "Run as administrator";
			this.isAdminMode.UseVisualStyleBackColor = true;
			// 
			// EditView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(407, 126);
			this.Controls.Add(this.isAdminMode);
			this.Controls.Add(this.AppGroup);
			this.Controls.Add(this.Info);
			this.Controls.Add(this.ButtonCancel);
			this.Controls.Add(this.Save);
			this.Controls.Add(this.AppPath);
			this.Controls.Add(this.AppName);
			this.Controls.Add(this.LabelGroup);
			this.Controls.Add(this.LabelPath);
			this.Controls.Add(this.LabelName);
			this.Controls.Add(this.ButtonBrowse);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "EditView";
			this.Text = "Quick Launcher - Edit";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LabelName;
		private System.Windows.Forms.Label LabelPath;
		private System.Windows.Forms.Label LabelGroup;
		private System.Windows.Forms.TextBox AppName;
		private System.Windows.Forms.TextBox AppPath;
		private System.Windows.Forms.Button ButtonBrowse;
		private System.Windows.Forms.Button Save;
		private System.Windows.Forms.Button ButtonCancel;
		private System.Windows.Forms.Label Info;
		private System.Windows.Forms.ComboBox AppGroup;
		private System.Windows.Forms.CheckBox isAdminMode;
	}
}

