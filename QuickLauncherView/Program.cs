﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuickLauncherView
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
			Controller controller = Controller.Instance;
			if (controller != null)
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);

				Application.Run(new CustomApplicationContext());
			}

		}
	}
}
