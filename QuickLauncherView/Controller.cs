﻿using System;
using QuickLauncher;

namespace QuickLauncherView
{
	public sealed class Controller
	{
		private static volatile Controller _instance;
		private static readonly object SyncRoot = new Object();

		private static Groups _groups;
		private static Applications _applications;

		private Controller()
		{
			_groups = new Groups();
			_applications = new Applications();
		}

		public static Controller Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (SyncRoot)
					{
						if (_instance == null)
						{
							_instance = new Controller();
						}
					}
				}

				return _instance;
			}
		}

		public static Applications GetApplicationsReference()
		{
			return _applications;
		}

		public static Groups GetGroupsReference()
		{
			return _groups;
		}
	}
}