﻿namespace QuickLauncherView
{
	partial class MainView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.NewApplication = new System.Windows.Forms.Button();
			this.applicationsView = new System.Windows.Forms.ListView();
			this.Close = new System.Windows.Forms.Button();
			this.Remove = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// NewApplication
			// 
			this.NewApplication.Location = new System.Drawing.Point(13, 576);
			this.NewApplication.Name = "NewApplication";
			this.NewApplication.Size = new System.Drawing.Size(75, 23);
			this.NewApplication.TabIndex = 1;
			this.NewApplication.Text = "New";
			this.NewApplication.UseVisualStyleBackColor = true;
			this.NewApplication.Click += new System.EventHandler(this.NewApplication_Click);
			// 
			// applicationsView
			// 
			this.applicationsView.Location = new System.Drawing.Point(12, 32);
			this.applicationsView.Name = "applicationsView";
			this.applicationsView.Size = new System.Drawing.Size(753, 538);
			this.applicationsView.TabIndex = 2;
			this.applicationsView.UseCompatibleStateImageBehavior = false;
			// 
			// Close
			// 
			this.Close.Location = new System.Drawing.Point(690, 576);
			this.Close.Name = "Close";
			this.Close.Size = new System.Drawing.Size(75, 23);
			this.Close.TabIndex = 3;
			this.Close.Text = "Close";
			this.Close.UseVisualStyleBackColor = true;
			this.Close.Click += new System.EventHandler(this.Close_Click);
			// 
			// Remove
			// 
			this.Remove.Location = new System.Drawing.Point(94, 576);
			this.Remove.Name = "Remove";
			this.Remove.Size = new System.Drawing.Size(75, 23);
			this.Remove.TabIndex = 4;
			this.Remove.Text = "Remove";
			this.Remove.UseVisualStyleBackColor = true;
			this.Remove.Click += new System.EventHandler(this.Remove_Click);
			// 
			// MainView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(777, 605);
			this.Controls.Add(this.Remove);
			this.Controls.Add(this.Close);
			this.Controls.Add(this.applicationsView);
			this.Controls.Add(this.NewApplication);
			this.Name = "MainView";
			this.Text = "MainView";
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button NewApplication;
		private System.Windows.Forms.ListView applicationsView;
		private System.Windows.Forms.Button Close;
		private System.Windows.Forms.Button Remove;
	}
}